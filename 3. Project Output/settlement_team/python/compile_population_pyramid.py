#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 24 12:04:34 2017

@author: dungchu
"""

#graph population pyramid
import pandas as pd
import os 

"""
path = '/Users/dungchu/Desktop/Civilise-AI/AuditLanding/Civilise\
-ENGN_AuditLanding/settlement_popping_materials/source_datasets'
"""

path = 'C:\\Users\\Dung Chu\\Desktop\\Civilise-ENGN_AuditLanding\\settlement_popping_materials\\source_datasets'
os.chdir(path)

erp_df_1996 = pd.read_csv('erp_1996_compiled.csv', sep = '\t', encoding = 'utf-8')
erp_df_2001 = pd.read_csv('erp_2001_compiled.csv', sep = '\t', encoding = 'utf-8')
erp_df_2006 = pd.read_csv('erp_2006_compiled.csv', sep = '\t', encoding = 'utf-8')
erp_df_2011 = pd.read_csv('erp_2011_compiled.csv', sep = '\t', encoding = 'utf-8')

def compile_pop_pyramid(in_df):
    in_df = in_df.sort('age')
    #out_df = pd.DataFrame()
    #out_df['age'] = in_df['age'].unique()
    female_df = in_df[in_df['gender'] == 'Females']
    male_df = in_df[in_df['gender'] == 'Males']
    #person_df = in_df[in_df['gender'] == 'Persons']
    temp_df = pd.DataFrame()
    temp_df['Males'] = list(male_df['Australia'])
    temp_df['Females'] = list(female_df['Australia'])
    #temp_df['Persons'] = list(person_df['Australia'])
    #out_df['Males %'] = temp_df['Males']/temp_df['Persons']
    #out_df['Females %'] = temp_df['Females']/temp_df['Persons']
    return temp_df


pop_pyramid_1996 = compile_pop_pyramid(erp_df_1996)
pop_pyramid_2001 = compile_pop_pyramid(erp_df_2001)
pop_pyramid_2006 = compile_pop_pyramid(erp_df_2006)
pop_pyramid_2011 = compile_pop_pyramid(erp_df_2011)