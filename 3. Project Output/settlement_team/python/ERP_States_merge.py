#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 10:59:16 2017

@author: dungchu
"""

#settlement popping data processing - Merge ERP States
import pandas as pd
import os 

def make_outdf_skeleton(filepath):
    states = ['Victoria', 'New South Wales', 'Australian Capital Territory',
              'South Australia', 'Western Australia', 'Tasmania', 'Queensland',
              'Northern Territory']
    erp_files = [f for f in os.listdir(filepath) if 'States' in f]
    f = erp_files[0]
    xls = pd.ExcelFile(f)
    sheets = xls.sheet_names
    df = xls.parse(sheets[-1])
    age_group, gender = df.iloc[5], df.iloc[6]
    age_zip_gender = zip(list(age_group),list(gender))
    index = pd.MultiIndex.from_tuples(list(age_zip_gender)[2:],
                                      names = ['age','gender'])
    out_df = pd.DataFrame(index = index, 
                          columns = states)
    return out_df
    
#find which states dataset is for
def find_states(sheet_df):
    found = 0
    states = ['Victoria', 'New South Wales', 'Australian Capital Territory',
              'South Australia', 'Western Australia', 'Tasmania', 'Queensland', 
              'Northern Territory']
    for i,r in sheet_df.iterrows():
        #find row with word 'Table' because it tells us which states
        if 'Table' in str(r[0]):
            row = r[0]
            found = 1
            break
    if found == 0: 
        return None #no state found
    for state in states:
        if state in row:
            return state

#compile data of different states into one dataframes
def fill_data(raw_df_path, out_df):
    raw_df = pd.read_excel(raw_df_path, sheetname = None)
    #loop through sheets i.e. states
    for sheet in raw_df.keys():
        if sheet != 'Contents':
            state = find_states(raw_df[sheet])
            print(sheet, state)
            raw_df_state = raw_df[sheet]
            raw_df_state = raw_df_state[5:] #drop first 5 rows
            col = list(zip(raw_df_state.iloc[0], raw_df_state.iloc[1]))
            raw_df_state.index = raw_df_state[raw_df_state.columns[1]]
            raw_df_state.columns = col
            for age_col in col[2:]:
                out_df.loc[age_col,state] = raw_df_state.loc['Total'][age_col]
    out_df = out_df.dropna(how = 'all')
    out_df = out_df.rename(columns = {None:'Australia'})
    return out_df
            
        
"""
path = '/Users/dungchu/Desktop/Civilise-AI/AuditLanding/Civilise\
-ENGN_AuditLanding/settlement_popping_materials/source_datasets'
"""
path = 'C:\\Users\\Dung Chu\\Desktop\\Civilise-ENGN_AuditLanding\\settlement_popping_materials\\source_datasets'
os.chdir(path)

erp_states_files = [f for f in os.listdir() if 'States' in f]

#compile 1996 Estimated Resident Population (ERP) data
erp_df_1996 = make_outdf_skeleton(path)
erp_df_1996 = fill_data(erp_states_files[0], erp_df_1996)

#compile 2001
erp_df_2001 = make_outdf_skeleton(path)
erp_df_2001 = fill_data(erp_states_files[1], erp_df_2001)

#compile 2011
erp_df_2006 = make_outdf_skeleton(path)
erp_df_2006 = fill_data(erp_states_files[2], erp_df_2006)

#compile 2016
erp_df_2011 = make_outdf_skeleton(path)
erp_df_2011 = fill_data(erp_states_files[3], erp_df_2011)

erp_df_1996.to_csv('erp_1996_compiled.csv', encoding = 'utf-8', sep = '\t')
erp_df_2001.to_csv('erp_2001_compiled.csv', encoding = 'utf-8', sep = '\t') 
erp_df_2006.to_csv('erp_2006_compiled.csv', encoding = 'utf-8', sep = '\t')
erp_df_2011.to_csv('erp_2011_compiled.csv', encoding = 'utf-8', sep = '\t')

  