What would you like to see? <br />
	*project_details.doc: overall project information <br />
	*Background Information folder: Group research <br />
	*Project Governance: How group manages tasks and deliverables <br />
	*Project Output: Group’s generated output <br />
	*Output Validation: Validate what Civilise.ENGN delivers with comparison to client’s requirements <br />
	*Handover: Compiled folder of outputs/future testings for client and shadows to access

